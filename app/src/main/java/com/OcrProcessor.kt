class OcrProcessor private constructor(private val context: Context) {

    private val textRecognizer: TextRecognizer = TextRecognizer.getClient(
        TextRecognizerOptions.Builder().setLanguage("zh-CN").build()
    )

    companion object {
        @JvmStatic
        fun getInstance(context: Context): OcrProcessor = Holder.instance

        private object Holder {
            private val instance = OcrProcessor(context.applicationContext)
        }
    }

    fun process(bitmap: Bitmap): String {
        return withContext(Dispatchers.IO) {
            val inputImage = InputImage.fromBitmap(bitmap, 1.0f)
            textRecognizer.process(inputImage).text
        }
    }
}