import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import org.opencv.android.OpenCVLoader
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.MatOfByte
import org.opencv.imgproc.Imgproc
import java.io.ByteArrayOutputStream

object BitmapUtils {

    // 初始化OpenCV库（需在Application中调用OpenCVLoader.initDebug()）
    init {
        if (!OpenCVLoader.initDebug()) {
            throw RuntimeException("OpenCV initialization failed")
        }
    }

    /**
     * 将Bitmap转换为OpenCV Mat对象
     */
    fun bitmapToMat(bitmap: Bitmap): Mat {
        val mat = Mat(bitmap.height, bitmap.width, CvType.CV_8UC4)
        val bytes = ByteArray(bitmap.byteCount)
        bitmap.getPixels(bytes, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)
        mat.put(0, 0, bytes)
        return mat
    }

    /**
     * 将OpenCV Mat对象转换回Bitmap
     */
    fun matToBitmap(mat: Mat): Bitmap {
        val bytes = ByteArray(mat.total() * mat.channels())
        mat.get(0, 0, bytes)
        return Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888).apply {
            setPixels(0, 0, mat.cols(), mat.rows(), bytes)
        }
    }

    /**
     * 高斯模糊处理（推荐使用OpenCV实现）
     */
    fun gaussianBlur(bitmap: Bitmap, kernelSize: Int = 3): Bitmap {
        val mat = bitmapToMat(bitmap)
        Imgproc.GaussianBlur(mat, mat, Size(kernelSize.toDouble(), kernelSize.toDouble()), 0.0)
        return matToBitmap(mat)
    }

    /**
     * 灰度化处理
     */
    fun grayscale(bitmap: Bitmap): Bitmap {
        val mat = bitmapToMat(bitmap)
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGBA2GRAY)
        return matToBitmap(mat)
    }

    /**
     * 自适应阈值处理（二值化）
     */
    fun binaryzation(bitmap: Bitmap, threshold: Double = 128.0): Bitmap {
        val mat = grayscale(bitmap)
        Imgproc.threshold(mat, mat, threshold, 255.0, 255.0, Imgproc.THRESH_BINARY_INV)
        return matToBitmap(mat)
    }

    /**
     * 倾斜校正（基于旋转角计算）
     */
    fun rotateImage(bitmap: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix().apply {
            postRotate(angle)
        }
        return Bitmap.createBitmap(
            bitmap,
            0,
            0,
            bitmap.width,
            bitmap.height,
            matrix,
            true
        )
    }

    /**
     * 车牌区域候选框绘制
     */
    fun drawCandidateRectangles(bitmap: Bitmap, rectangles: List<Rect>): Bitmap {
        val result = bitmap.copy(Bitmap.Config.ARGB_8888, true)
        val canvas = Canvas(result)
        val paint = Paint().apply {
            color = Color.YELLOW
            strokeWidth = 5f
            style = Paint.Style.STROKE
        }
        for (rect in rectangles) {
            canvas.drawRect(rect.left, rect.top, rect.right, rect.bottom, paint)
        }
        return result
    }

    /**
     * 车牌字符分割（基于投影法）
     */
    fun splitLicensePlateChars(bitmap: Bitmap): List<Bitmap> {
        val mat = bitmapToMat(bitmap)
        val grayMat = grayscale(mat)
        val verticalProjection = IntArray(grayMat.cols())

        // 计算垂直投影
        for (col in 0 until grayMat.cols()) {
            var sum = 0
            for (row in 0 until grayMat.rows()) {
                sum += grayMat.get(row, col)[0]
            }
            verticalProjection[col] = sum
        }

        // 确定字符区域
        val regions = []
        var start = 0
        while (start < verticalProjection.size) {
            while (start < verticalProjection.size && verticalProjection[start] == 0) start++
            if (start >= verticalProjection.size) break
            var end = start
            while (end < verticalProjection.size && verticalProjection[end] > 0) end++
            regions.add(start to end - 1)
            start = end
        }

        // 提取每个字符区域
        return regions.map { (startCol, endCol) ->
            Bitmap.createBitmap(
                bitmap,
                0,
                0,
                bitmap.width,
                bitmap.height,
                Rect(0, 0, endCol, bitmap.height)
            )
        }
    }

    /**
     * 车牌倾斜角度计算
     */
    fun calculateSkewAngle(mat: Mat): Double {
        val lines = Imgproc.findLines(mat)
        if (lines.isEmpty()) return 0.0

        val angles = mutableListOf<Double>()
        for (line in lines) {
            val rho = line[0]
            val theta = line[1]
            angles.add(theta)
        }

        // 统计出现频率最高的倾斜角
        val frequencyMap = angles.groupingBy { it }
            .mapValues { it.value.count() }
            .sortedByDescending { it.value }

        return frequencyMap.firstOrNull()?.key ?: 0.0
    }

    /**
     * 优化后的位图缩放（保持长宽比）
     */
    fun scaleBitmap(bitmap: Bitmap, targetWidth: Int, targetHeight: Int): Bitmap {
        val ratio = Math.min(
            targetWidth.toDouble() / bitmap.width,
            targetHeight.toDouble() / bitmap.height
        )
        return Bitmap.createScaledBitmap(
            bitmap,
            (bitmap.width * ratio).toInt(),
            (bitmap.height * ratio).toInt(),
            true
        )
    }
}