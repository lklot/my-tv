class CameraActivity : AppCompatActivity(), TextureView.SurfaceTextureListener {

    private lateinit var cameraExecutor: ExecutorService
    private lateinit var imageCapture: ImageCapture
    private lateinit var textureView: TextureView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)

        textureView = findViewById(R.id.texture_view)
        textureView.surfaceTextureListener = this

        // 初始化相机组件
        cameraExecutor = Executors.newSingleThreadExecutor()
        val cameraProvider = ProcessCameraProvider.getInstance(this)
        cameraProvider.unbindAll()
        startCamera()
    }

    private fun startCamera() {
        val cameraSelector = CameraSelector.Builder().requireLensFacing(
            CameraSelector.LENS_FACING_BACK
        ).build()

        try {
            val camera = cameraProvider.bindToLifecycle(
                this,
                cameraSelector,
                object : ImageAnalysis.Analyzer { /* ... */ },
                object : ImageCapture.OnImageCapturedCallback { 
                    override fun onImageCaptured(
                        image: Image,
                        timestamp: Long
                    ) {
                        val bitmap = image.toBitmap()
                        val resultText = OcrProcessor.process(bitmap)
                        runOnUiThread {
                            setResult(RESULT_OK, Intent().putExtra("bitmap", bitmap))
                            finish()
                        }
                    }
                }
            )
        } catch (e: Exception) {
            Log.e("CameraActivity", "Error starting camera", e)
        }
    }

    override fun onSurfaceTextureAvailable(
        surface: SurfaceTexture,
        width: Int,
        height: Int
    ) {
        // 初始化相机预览
    }

    override fun onSurfaceTextureSizeChanged(
        surface: SurfaceTexture,
        width: Int,
        height: Int
    ) {
        // 处理尺寸变化
    }

    override fun onSurfaceTextureDestroyed(surface: SurfaceTexture) {
        cameraExecutor.shutdown()
    }

    override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {
        // 更新预览画面
    }
}