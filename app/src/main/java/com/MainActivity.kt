class MainActivity : AppCompatActivity() {

    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_PERMISSIONS = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // 初始化UI组件
        findViewById<Button>(R.id.scan_button).setOnClickListener {
            if (allPermissionsGranted()) {
                startCameraActivity()
            } else {
                requestPermissions()
            }
        }
    }

    private fun allPermissionsGranted() = 
        PackageManager.PERMISSION_GRANTED == 
            checkSelfPermission(Manifest.permission.CAMERA)

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.CAMERA),
            REQUEST_PERMISSIONS
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_PERMISSIONS && grantResults.isNotEmpty() && 
            grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startCameraActivity()
        } else {
            Toast.makeText(this, "需要相机权限", Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    private fun startCameraActivity() {
        val intent = Intent(this, CameraActivity::class.java)
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            val resultText = processImage(imageBitmap)
            showResult(resultText)
        }
    }

    private fun processImage(bitmap: Bitmap): String {
        return OcrProcessor.process(bitmap)
    }

    private fun showResult(text: String) {
        findViewById<TextView>(R.id.result_text).text = text
        findViewById<ImageView>(R.id.highlight_view).visibility = 
            if (PlateValidator.isLucky(text)) View.VISIBLE else View.GONE
    }
}