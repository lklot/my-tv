class PlateValidator private constructor(private val rules: List<Rule>) {

    companion object {
        @JvmStatic
        fun getDefaultRules(context: Context): PlateValidator {
            return PlateValidator.fromSharedPreferences(
                context.getSharedPreferences("app_rules", Context.MODE_PRIVATE)
            )
        }
    }

    fun isLucky(number: String): Boolean {
        return number.matchesAnyRule()
    }

    private fun String.matchesAnyRule(): Boolean {
        return rules.any { 
            when (it.type) {
                Rule.TYPE_SIMPLE -> contains(it.pattern)
                Rule.TYPE_REGEX -> Regex(it.pattern).matches(this)
            }
        }
    }

    private class Rule(val pattern: String, val type: Int)

    private fun fromSharedPreferences(sharedPreferences: SharedPreferences): PlateValidator {
        val patternList = sharedPreferences.getStringSet("rules", emptySet()) ?: setOf()
        val useRegex = sharedPreferences.getBoolean("use_regex", false)
        return PlateValidator(
            patternList.map { 
                Rule(it, if (useRegex) Rule.TYPE_REGEX else Rule.TYPE_SIMPLE) 
            }
        )
    }
}